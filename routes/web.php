<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TablesController;
use App\Http\Controllers\TypographyController;
use App\Http\Controllers\IconsController;
use App\Http\Controllers\MapController;
use App\Http\Controllers\NotificationsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'index']);
Route::get('/user', [UserController::class, 'index']);
Route::get('/tables', [TablesController::class, 'index']);
Route::get('/typography', [TypographyController::class, 'index']);
Route::get('/icons', [IconsController::class, 'index']);
Route::get('/map', [MapController::class, 'index']);
Route::get('/notifications', [NotificationsController::class, 'index']);
